import inherits from 'inherits';

import ContextPadProvider from 'bpmn-js/lib/features/context-pad/ContextPadProvider';

import {
  isAny
} from 'bpmn-js/lib/features/modeling/util/ModelingUtil';

import {
  assign,
  bind
} from 'min-dash';


export default function CustomContextPadProvider(injector, connect, popupMenu, translate, elementFactory, config) {

  injector.invoke(ContextPadProvider, this);

  this._popupMenu = popupMenu;
  this._elementFactory = elementFactory;
  this._config = config;

  if (config.autoPlace !== false) {
    this._autoPlace = injector.get('autoPlace', false);
  }

  var cached = bind(this.getContextPadEntries, this);

  this.getContextPadEntries = function(element) {
    var actions = cached(element);

    var popupMenu = this._popupMenu;
    var elementFactory = this._elementFactory;
    var autoPlace = this._autoPlace;

    var businessObject = element.businessObject;

    function appendAction(type, className, title, options) {

      if (typeof title !== 'string') {
        options = title;
        title = translate('Append {type}', { type: type.replace(/^bpmn:/, '') });
      }
    
      function appendStart(event, element) {
    
        var shape = elementFactory.createShape(assign({ type: type }, options));
        create.start(event, shape, element);
      }
    
    
      var append = autoPlace ? function(event, element) {
        var shape = elementFactory.createShape(assign({ type: type }, options));
    
        autoPlace.append(element, shape);
      } : appendStart;
    
    
      return {
        group: 'model',
        className: className,
        title: title,
        action: {
          dragstart: appendStart,
          click: append
        }
      };
    }

    function startConnect(event, element, autoActivate) {
      connect.start(event, element, autoActivate);
    }

    if (isAny(businessObject, ['bpmn:Task'])) {
      assign(actions, {
        'append.receive-task': appendAction(
          'bpmn:ReceiveTask',
          'bpmn-icon-receive-task'
        ),
        'append.message-intermediate-event': appendAction(
          'bpmn:IntermediateCatchEvent',
          'bpmn-icon-intermediate-event-catch-message',
          { eventDefinitionType: 'bpmn:MessageEventDefinition' }
        ),
        'append.timer-intermediate-event': appendAction(
          'bpmn:IntermediateCatchEvent',
          'bpmn-icon-intermediate-event-catch-timer',
          { eventDefinitionType: 'bpmn:TimerEventDefinition' }
        ),
        'append.condtion-intermediate-event': appendAction(
          'bpmn:IntermediateCatchEvent',
          'bpmn-icon-intermediate-event-catch-condition',
          { eventDefinitionType: 'bpmn:ConditionalEventDefinition' }
        ),
        'append.signal-intermediate-event': appendAction(
          'bpmn:IntermediateCatchEvent',
          'bpmn-icon-intermediate-event-catch-signal',
          { eventDefinitionType: 'bpmn:SignalEventDefinition' }
        ),
        'append.etalon-labor': appendAction(
          'bpmn:UserTask',
          'etalon-icon-logo'
        )
      });
    }

    if (isAny(businessObject, [ 'custom:triangle', 'custom:circle'])) {
      assign(actions, {
        'connect': {
          group: 'connect',
          className: 'bpmn-icon-connection-multi',
          title: translate('Connect using custom connection'),
          action: {
            click: startConnect,
            dragstart: startConnect
          }
        }
      });
    }

    return actions;
  };
}

inherits(CustomContextPadProvider, ContextPadProvider);

CustomContextPadProvider.$inject = [
  'injector',
  'connect',
  'popupMenu',
  'translate',
  'elementFactory',
  'config'
];