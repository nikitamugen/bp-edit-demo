import ReplaceModule from '../custom-replace';

import CustomReplaceMenuProvider from './CustomReplaceMenuProvider';


export default {
    __depends__: [
        ReplaceModule
    ],
    __init__: ['replaceMenuProvider'],
    replaceMenuProvider: ['type', CustomReplaceMenuProvider]
};