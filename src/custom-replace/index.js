import BpmnReplace from 'bpmn-js/lib/features/replace/BpmnReplace';

export default {
    bpmnReplace: ['type', BpmnReplace]
};