require("../style/app.less");
require("../style/icon.less");

var container = $('#js-drop-zone');

import 'bpmn-js/dist/assets/diagram-js.css';
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn.css';

import $ from 'jquery';
import _ from 'lodash';
import { debounce } from 'min-dash';

import MiniMapModule from 'diagram-js-minimap';

import ResizeAllRuleModule from './rule';

import PropertiesPanelModule from 'bpmn-js-properties-panel';
import PropertiesProviderModule from './provider';
import etalonModdleExtension from './moddle-extension/etalon-extension';

import customLocale from './i18n/customLocale';
var localeModule = {
    translate: ['value', customLocale]
};

import CustomModeler from './custom-modeler';
import CustomPopupMenu from './custom-popup-menu';
var bpmnModeler = new CustomModeler({
    container: '#js-canvas',
    propertiesPanel: {
        parent: '#js-properties-panel'
    },
    keyboard: {
        bindTo: document
    },
    additionalModules: [
        MiniMapModule,
        PropertiesProviderModule,
        PropertiesPanelModule,
        ResizeAllRuleModule,
        localeModule,
        CustomPopupMenu
    ],
    moddleExtensions: {
        etalon: etalonModdleExtension
    }
});

$(".djs-minimap")
    .addClass("mdi")
    .addClass("mdi-set");

import diagramXML from '../bpmn/diagram.bpmn';

function createNewDiagram() {
    openDiagram(diagramXML);
}

function openDiagram(xml) {

    bpmnModeler.importXML(xml, function(err) {

        if (err) {
            container
                .removeClass('with-diagram')
                .addClass('with-error');

            container.find('.error pre').text(err.message);

            console.error(err);
        } else {
            container
                .removeClass('with-error')
                .addClass('with-diagram');
        }
    });
}

function saveSVG(done) {
    bpmnModeler.saveSVG(done);
}

function saveDiagram(done) {

    bpmnModeler.saveXML({ format: true }, function(err, xml) {
        done(err, xml);
    });
}

function registerFileDrop(container, callback) {

    function handleFileSelect(e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.dataTransfer.files;

        var file = files[0];

        var reader = new FileReader();

        reader.onload = function(e) {

            var xml = e.target.result;

            callback(xml);
        };

        reader.readAsText(file);
    }

    function handleDragOver(e) {
        e.stopPropagation();
        e.preventDefault();

        e.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    container.get(0).addEventListener('dragover', handleDragOver, false);
    container.get(0).addEventListener('drop', handleFileSelect, false);
}


////// file drag / drop ///////////////////////

// check file api availability
if (!window.FileList || !window.FileReader) {
    window.alert(
        'Looks like you use an older browser that does not support drag and drop. ' +
        'Try using Chrome, Firefox or the Internet Explorer > 10.');
} else {
    registerFileDrop(container, openDiagram);
}

// bootstrap diagram functions

$(function() {

    $('#js-create-diagram').click(function(e) {
        e.stopPropagation();
        e.preventDefault();

        createNewDiagram();
    });

    var downloadLink = $('#js-download-diagram');
    var downloadSvgLink = $('#js-download-svg');

    $('.buttons a').click(function(e) {
        if (!$(this).is('.active') &&
            !$(this).is('.mode-button')) {
            e.preventDefault();
            e.stopPropagation();
        }
    });

    function setEncoded(link, name, data) {
        var encodedData = encodeURIComponent(data);

        if (data) {
            link.addClass('active').attr({
                'href': 'data:application/bpmn20-xml;charset=UTF-8,' + encodedData,
                'download': name
            });
        } else {
            link.removeClass('active');
        }
    }

    var exportArtifacts = debounce(function() {

        saveSVG(function(err, svg) {
            setEncoded(downloadSvgLink, 'diagram.svg', err ? null : svg);
        });

        saveDiagram(function(err, xml) {
            setEncoded(downloadLink, 'diagram.bpmn', err ? null : xml);
        });
    }, 500);

    bpmnModeler.on('commandStack.changed', exportArtifacts);
});