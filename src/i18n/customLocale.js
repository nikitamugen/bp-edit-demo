import localeRu from './ru-RU';

export default function customLocale(template, replacements) {
    replacements = replacements || {};

    // Translate
    template = localeRu[template] || template;

    // Replace
    return template.replace(/{([^}]+)}/g, function(_, key) {
        return replacements[key] || '{' + key + '}';
    });
}